(function checkForCompatibleBrowser() {
  // https://stackoverflow.com/questions/10964966/detect-ie-version-prior-to-v9-in-javascript
  var div = document.createElement("div");
  div.innerHTML = "<!--[if lt IE 9]><i></i><![endif]-->";
  var isIeLessThan9 = (div.getElementsByTagName("i").length == 1);
  if (isIeLessThan9) {
    window.location.replace("errorGraphicsNotSupported.html"); // Prevent back button going to same error page and coming back here infinitely.
  }
})();