/**
 * Created by Dean on 2/9/2017.
 */
function getRandomColor(opacity) {
    var r = getRandomIntInclusive(0,255);
    var g = getRandomIntInclusive(0,255);
    var b = getRandomIntInclusive(0,255);
    var color = 'rgba(' + r + ', ' + g + ', ' + b + ', ' + opacity + ')';
    return color;
}

